package main

import (
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
)

type nfe struct {
	XMLName xml.Name `xml:"nfeProc"`
	InfNfe  infNfe   `xml:"NFe>infNFe"`
}

type infNfe struct {
	Id    string  `xml:"Id,attr"`
	CNPJ  string  `xml:"emit>CNPJ"`
	Valor float64 `xml:"total>ICMSTot>vNF"`
}

type saveRequest struct {
	XML string
}

type saveResponse struct {
	IsNewNfe bool
}

type getResponse struct {
	XML string
}

var m map[string]nfe

func save(w http.ResponseWriter, r *http.Request) {

	content, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err)
	}

	var req saveRequest
	if err = json.Unmarshal(content, &req); err != nil {
		panic(err)
	}

	xmlContent, err := base64.StdEncoding.DecodeString(req.XML)
	if err != nil {
		panic(err)
	}

	var nfe nfe
	if err = xml.Unmarshal(xmlContent, &nfe); err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")

	var resp saveResponse
	if _, exists := m[nfe.InfNfe.Id]; exists {
		resp.IsNewNfe = false
	} else {
		m[nfe.InfNfe.Id] = nfe
		resp.IsNewNfe = true
	}

	json.NewEncoder(w).Encode(resp)
}

func get(w http.ResponseWriter, r *http.Request) {

	ids, ok := r.URL.Query()["id"]

	if !ok || len(ids[0]) < 1 {
		log.Println("Url Param 'id' is missing")
		return
	}

	id := ids[0]

	var resp getResponse
	if _, exists := m[id]; exists {
		nfe := m[id]

		xmlContent, err := xml.Marshal(nfe)
		if err != nil {
			panic(err)
		}

		resp.XML = base64.StdEncoding.EncodeToString(xmlContent)

		w.Header().Set("Content-Type", "application/json")

		json.NewEncoder(w).Encode(resp)
	} else {
		w.WriteHeader(404)
	}
}

func main() {

	m = make(map[string]nfe)

	http.HandleFunc("/nfse/v1", save)
	http.HandleFunc("/get", get)

	log.Fatal(http.ListenAndServe(":8000", nil))
}
